<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\UserDetails;
use App\Models\UserAddress;
use App\Models\PageLoadDetails;
use View;
use Jenssegers\Agent\Facades\Agent;


class RegistrationController extends Controller
{
    public function registration(Request $request) {

      //$ip = $request->header('User-Agent');
      $ip = get_browser( $request->header('User-Agent') , true);
      $browser = Agent::browser();
      $version = Agent::version($browser);
      $device = Agent::device();
      $brower_data = $browser.$version;
      $user_agent = Agent::platform();
      $device_type = "";
      if (Agent::isMobile()) {
            $device_type = 'Mobile.';
        }else if (Agent::isDesktop()) {
            $device_type = 'Desktop.';
        }else if (Agent::isTablet()) {
            $device_type = 'Desktop.';
        }else if (Agent::isPhone()) {
            $device_type = 'Phone.';
        }
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        $user_details = new PageLoadDetails;
        $user_details->ipaddress = $ipaddress;
        $user_details->device_type = $device_type;
        $user_details->brower = $brower_data;
        $user_details->user_agent = $user_agent;
        $user_details->save();

      return View::make('registration');
    }

    public function store(Request $request)
    {
      $request->flash();
      if(!isset($request['next']) && !isset($request['submit'])) {
        Session::flash('message', 'Something went wrong!');
        Session::flash('alert-class', 'alert-danger');
        return Redirect::back();
      }

      if($request['submit']) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'day' => 'required|numeric|min:1|max:31',
            'month' => 'required|numeric|min:1|max:12',
            'year' => 'required|numeric|min:1910|max:2002',
            'email' => 'required|email|unique:user_details,email',
            'phone_number' => 'required|numeric|digits:10|unique:user_details,phone_number'
        ]);

        if ($validator->fails()) {
            Session::flash('first_form', 'okay');
            return Redirect::back()->withErrors($validator);
        } else {
          $user_details = new UserDetails;
          $user_details->first_name = $request->first_name;
          $user_details->last_name = $request->last_name;
          $user_details->dob = $request->year.'-'.$request->month.'-'.$request->day;
          $user_details->email = $request->email;
          $user_details->phone_number = $request->phone_number;
          $user_details->save();
          $first_name = $request->first_name;
          $last_user_id = $user_details->id;
          return View::make('page-two', compact(['first_name','last_user_id']));

        }

      } elseif($request['next']) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'day' => 'required|numeric|min:1|max:31',
            'month' => 'required|numeric|min:1|max:12',
            'year' => 'required|numeric|min:1910|max:2002',
        ]);

        if ($validator->fails()) {
            Session::flash('first_form', 'not_okay');
        } else {
            Session::flash('first_form', 'okay');
        }
        return Redirect::back()->withErrors($validator);
      }

    }

    public function address_store(Request $request) {
      $user_id = $request->user_id;
      if(!$user_id) {
        Session::flash('message', 'Something went wrong!');
        Session::flash('alert-class', 'alert-danger');
        return Redirect::back();
      }
      $request->flash();
      $validator = Validator::make($request->all(), [
          "address_line_one.*"  => "required_with:address_line_three.*|nullable|string|distinct|min:3|max:50",
          "address_line_two.*"  => "required_with:address_line_one.*|nullable|string|distinct|min:3|max:50",
          "address_line_three.*"  => "required_with:address_line_two.*|nullable|string|distinct|min:3|max:50"
      ]);

      if ($validator->fails()) {
          Session::flash('page_two_form', 'not_okay');
          $first_name = $request->first_name;
          $last_user_id = $request->user_id;
          return View::make('page-two', compact(['first_name','last_user_id']))->withErrors($validator);
          //return Redirect::back()->withErrors($validator);
      } else {
          Session::flash('page_two_form', 'okay');

          $user_details = UserDetails::where('id', $user_id)->first();
          if(!$user_details) {
            Session::flash('message', 'Something went wrong!');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::back();
          }
          UserAddress::where('id', $user_id)->delete();

          foreach($request->address_line_one as $key => $val) {
            if($val != "") {
              $user_details = new UserAddress;
              $user_details->line_one = $val;
              $user_details->line_two = $request->address_line_two[$key];
              $user_details->line_three = $request->address_line_three[$key];
              $user_details->save();
            }            
          }

      }
      return redirect()->route('thankyou');

    }

}
