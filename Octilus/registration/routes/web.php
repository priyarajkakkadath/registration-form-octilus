<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/thankyou', function () {
    return view('thankyou');
    //return view('welcome');
})->name('thankyou');
Route::get('/page-two', function () {
    return view('page-two');
    //return view('welcome');
})->name('page-two');

Route::get('/', [RegistrationController::class, 'registration'])->name('registration');
Route::post('/', [RegistrationController::class, 'store'])->name('store');
Route::post('/address-store', [RegistrationController::class, 'address_store'])->name('address-store');

//Route::post('/store','RegistrationController@store')->name('store');
