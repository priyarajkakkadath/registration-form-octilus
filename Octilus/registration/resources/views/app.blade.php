<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <!-- Title -->
      <title>Project</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Meta description -->

      <!-- CSS -->
      <!-- Favicon -->
      <link rel="icon" type="image/png" href="dist/img/favicon.ico">
      <link rel="stylesheet" href="{{ asset('css/main.css') }}">
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/reg_app.js') }}"></script>
      <script src="{{ asset('js/simple-slider.js') }}"></script>
   </head>
