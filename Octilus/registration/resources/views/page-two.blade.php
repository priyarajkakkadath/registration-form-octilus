@include('app')

    <!-- body start -->
   <body>
      <header>
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-12 text-center">
                  <img src="{{ asset('img/logo.png') }}" alt="">
               </div>
            </div>
         </div>
      </header>

      <section class="bnrsection">
         <!-- <div class="container-fluid">
            <div class="row">
               <div class="col-lg-12 p-0">
                  <img src="dist/img/bnr.jpg" alt="">
               </div>
            </div>
         </div> -->
         <div class="container">
            <div class="row">
               <div class="offset-lg-1 col-lg-10 col-md-12 col-12 text-center">
                  <h1>Hi <span>{{$first_name}}</span> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h1>
                  <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
               </div>
               <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10 col-12 text-center">
                  <div class="formpart">
                      <form action="{{route('address-store')}}" method="POST" id="formId">
                      @csrf
                        <div id="slide03" style="display: block;">
                           <h3>Do you have a Previous Address?</h3>
                           <div class="form-check">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label next02" for="flexRadioDefault1">
                              Yes
                            </label>
                          </div>
                          <div class="form-check" onclick="go_to_thankyou()">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                            <label class="form-check-label" for="flexRadioDefault2">
                              No
                            </label>
                          </div>
                        </div>

                        <div id="slide04" style="display:none;">
                           <h3>Enter your Previous Address</h3>
                           @if(Session::has('message'))
                           <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                          @endif
                            <input type="hidden" value="{{$last_user_id}}" name="user_id">
                            <input type="hidden" value="{{$first_name}}" name="first_name">
                            <div class="mb-3 text-start">
                                <label class="form-label">Previous Address 1</label>
                                <input type="text" name="address_line_one[]" value="{{old('address_line_one.0')}}" class="form-control mt-3" id="" placeholder="Address line 1">
                                @if ($errors->has('address_line_one.0'))
                                    <span class="text-danger">{{ $errors->first('address_line_one.0') }}</span>
                                @endif
                                <input type="text" name="address_line_two[]" value="{{old('address_line_two.0')}}" class="form-control mt-3" id="" placeholder="Address line 2">
                                @if ($errors->has('address_line_two.0'))
                                    <span class="text-danger">{{ $errors->first('address_line_two.*') }}</span>
                                @endif
                                <input type="text" name="address_line_three[]" value="{{old('address_line_three.0')}}" class="form-control mt-3" id="" placeholder="Address line 3">
                                @if ($errors->has('address_line_three.0'))
                                    <span class="text-danger">{{ $errors->first('address_line_three.*') }}</span>
                                @endif
                            </div>

                            <div class="mb-3 text-center" id="submitoradd01">
                                <button type="button" class="btn btn-success" onclick="go_to_submit()">Submit</button>
                                <p><a href="#postaddrs2" id="showadrs2">Add Another Address</a></p>
                                <p><a href="#postaddrs2" id="back02"><< Back</a></p>
                            </div>


                            <div id="postaddrs2" style="display:none">
                                <div class="mb-3 text-start">
                                    <label for="" class="form-label">Previous Address 2</label>
                                    <input type="text" name="address_line_one[]" value="{{old('address_line_one.1')}}" class="form-control mt-3" id="" placeholder="Address line 1">
                                    @if ($errors->has('address_line_one.1'))
                                        <span class="text-danger">{{ $errors->first('address_line_one.1') }}</span>
                                    @endif
                                    <input type="text" name="address_line_two[]" value="{{old('address_line_two.1')}}" class="form-control mt-3" id="" placeholder="Address line 2">
                                    @if ($errors->has('address_line_two.1'))
                                        <span class="text-danger">{{ $errors->first('address_line_two.1') }}</span>
                                    @endif
                                    <input type="text" name="address_line_three[]" value="{{old('address_line_three.1')}}" class="form-control mt-3" id="" placeholder="Address line 3">
                                    @if ($errors->has('address_line_three.1'))
                                        <span class="text-danger">{{ $errors->first('address_line_three.1') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3 text-center" id="submitoradd02">
                                    <button type="button" class="btn btn-success" onclick="go_to_submit()">Submit</button>
                                    <p><a href="#postaddrs3" id="showadrs3">Add Another Address</a></p>
                                    <p><a href="#slide04" id="remove3">Remove Address</a></p>
                                </div>
                            </div>


                            <div id="postaddrs3" style="display:none">
                                <div class="mb-3 text-start">
                                    <label for="" class="form-label">Previous Address 3</label>
                                    <input type="text" name="address_line_one[]" value="{{old('address_line_one.2')}}" class="form-control mt-3" id="" placeholder="Address line 1">
                                    @if ($errors->has('address_line_one.2'))
                                        <span class="text-danger">{{ $errors->first('address_line_one.2') }}</span>
                                    @endif
                                    <input type="text" name="address_line_two[]" value="{{old('address_line_one.2')}}" class="form-control mt-3" id="" placeholder="Address line 2">
                                    @if ($errors->has('address_line_two.2'))
                                        <span class="text-danger">{{ $errors->first('address_line_two.2') }}</span>
                                    @endif
                                    <input type="text" name="address_line_three[]" value="{{old('address_line_one.2')}}" class="form-control mt-3" id="" placeholder="Address line 3">
                                    @if ($errors->has('address_line_three.2'))
                                        <span class="text-danger">{{ $errors->first('address_line_three.2') }}</span>
                                    @endif
                                </div>

                                <div class="mb-3 text-center">
                                    <button type="button" class="btn btn-success" onclick="go_to_submit()">Submit</button>
                                    <p><a href="#slide04" id="remove4">Remove Address</a></p>
                                </div>
                            </div>


                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-12">
                  <h5>Lorem Ipsum is simply dummy text</h5>
                  <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                  <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                  <p>Copyright@2022</p>
               </div>
            </div>
         </div>
      </footer>

      <script>
      function go_to_thankyou() {
        console.log('ffffffff');
        window.location.href = "{{ url('thankyou') }}";
      }

      function go_to_submit() {
        console.log('dddddddddd');
        $("#formId").submit();
      }

      $( document ).ready(function() {

          console.log( "ready!" );
          var page_two_form = "";
          @if(Session::has('page_two_form'))
            page_two_form = "{{ Session::get('page_two_form') }}";
          @endif

          if(page_two_form == "") {
              $("#slide04").hide();
              $("#slide03").show();
          } else if(page_two_form == "not_okay") {
              $("#slide04").show();
              $("#slide03").hide();
          } else {
              $("#slide04").hide();
              $("#slide03").show();
          }

      });

      </script>

   </body>
    <!--body end -->

</html>
