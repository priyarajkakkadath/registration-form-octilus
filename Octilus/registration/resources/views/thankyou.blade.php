@include('app')
    <!-- body start -->
   <body>
      <header>
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-12 text-center">
                  <img src="{{ asset('img/logo.png') }}" alt="">
               </div>
            </div>
         </div>
      </header>

      <section class="bnrsection">
         <!-- <div class="container-fluid">
            <div class="row">
               <div class="col-lg-12 p-0">
                  <img src="dist/img/bnr.jpg" alt="">
               </div>
            </div>
         </div> -->
         <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-10 col-md-12 col-12 text-center">
                    <h2>Thankyou...</h2>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                 </div>
            </div>
        </div>
    </section>



    <footer class="fixed-bottom">
        <div class="container">
           <div class="row">
              <div class="col-lg-12 col-12">
                 <h5>Lorem Ipsum is simply dummy text</h5>
                 <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                 <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                 <p>Copyright@2022</p>
              </div>
           </div>
        </div>
     </footer>

  </body>
   <!--body end -->

</html>
